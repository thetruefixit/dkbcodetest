package de.dkb.dkbhometest.data.network

import de.dkb.dkbhometest.data.model.ImageDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ImageApiService {
    @GET("/photos")
    suspend fun getAllPhotos(): Response<List<ImageDto>>

    @GET("/photos/{id}")
    suspend fun getPhoto(@Path("id") id: Int): Response<ImageDto>
}