package de.dkb.dkbhometest.data.sources

import de.dkb.dkbhometest.data.model.ImageDto

interface ImageDataSource {
    suspend fun getAllImages(): List<ImageDto>
    suspend fun getImage(id: Int): ImageDto
}