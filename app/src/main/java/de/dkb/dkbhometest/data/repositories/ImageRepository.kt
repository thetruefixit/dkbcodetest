package de.dkb.dkbhometest.data.repositories

import de.dkb.dkbhometest.presentation.model.Image

interface ImageRepository {
    suspend fun getAllImages(): List<Image>
    suspend fun getImage(id: Int): Image
}