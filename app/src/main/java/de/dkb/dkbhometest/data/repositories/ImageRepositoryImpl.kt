package de.dkb.dkbhometest.data.repositories

import de.dkb.dkbhometest.data.sources.RemoteImageDataSource
import de.dkb.dkbhometest.presentation.model.Image
import de.dkb.dkbhometest.presentation.model.ImageMapper.toInternalObject
import javax.inject.Inject

class ImageRepositoryImpl @Inject constructor(
    private val remoteImageDataSource: RemoteImageDataSource
) : ImageRepository {
    override suspend fun getAllImages(): List<Image> =
        remoteImageDataSource.getAllImages().map { it.toInternalObject() }

    override suspend fun getImage(id: Int): Image =
        remoteImageDataSource.getImage(id).toInternalObject()
}