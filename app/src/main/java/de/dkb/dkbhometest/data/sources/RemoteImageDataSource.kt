package de.dkb.dkbhometest.data.sources

import de.dkb.dkbhometest.data.model.ImageDto
import de.dkb.dkbhometest.data.network.ImageApiService
import de.dkb.dkbhometest.utils.CoroutinesManager
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RemoteImageDataSource @Inject constructor(
    private val api: ImageApiService,
    private val coroutinesManager: CoroutinesManager
) : ImageDataSource {
    override suspend fun getAllImages(): List<ImageDto> = withContext(coroutinesManager.io) {
        val response = api.getAllPhotos()
        val data = response.body()
        if (response.isSuccessful && data != null) {
            return@withContext data
        } else {
            throw RuntimeException("Can't get data from the remote data source")
        }
    }

    override suspend fun getImage(id: Int): ImageDto = withContext(coroutinesManager.io) {
        val response = api.getPhoto(id)
        val data = response.body()
        if (response.isSuccessful && data != null) {
            return@withContext data
        } else {
            throw RuntimeException("Can't get data from the remote data source")
        }
    }
}