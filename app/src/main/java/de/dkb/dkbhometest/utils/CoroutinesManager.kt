package de.dkb.dkbhometest.utils

import kotlin.coroutines.CoroutineContext

interface CoroutinesManager {
    val main: CoroutineContext
    val io: CoroutineContext
}