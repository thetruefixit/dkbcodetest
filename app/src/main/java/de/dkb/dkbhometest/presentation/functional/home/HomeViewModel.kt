package de.dkb.dkbhometest.presentation.functional.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.dkb.dkbhometest.core.other.ScreenState
import de.dkb.dkbhometest.data.repositories.ImageRepository
import de.dkb.dkbhometest.presentation.model.Image
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val imageRepository: ImageRepository
) : ViewModel() {
    private val _screenState: MutableStateFlow<ScreenState<List<Image>>> =
        MutableStateFlow(ScreenState.Loading)

    init {
        requestAllImages()
    }

    fun onRetryClicked() {
        requestAllImages()
    }

    private fun requestAllImages() {
        viewModelScope.launch {
            _screenState.update { ScreenState.Loading }
            try {
                val images = imageRepository.getAllImages()
                if (images.isNotEmpty()) {
                    _screenState.update { ScreenState.Content(images) }
                } else {
                    _screenState.update { ScreenState.Error(NoSuchElementException("No items to show")) }
                }
            } catch (exception: Throwable) {
                exception.printStackTrace()
                _screenState.update { ScreenState.Error(exception) }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PUBLIC STATES
    ///////////////////////////////////////////////////////////////////////////

    val screenState = _screenState.asStateFlow()
}