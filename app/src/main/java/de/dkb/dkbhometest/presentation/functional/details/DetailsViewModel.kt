package de.dkb.dkbhometest.presentation.functional.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.dkb.dkbhometest.core.other.ScreenState
import de.dkb.dkbhometest.data.repositories.ImageRepository
import de.dkb.dkbhometest.presentation.model.Image
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val imageRepository: ImageRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val _screenState: MutableStateFlow<ScreenState<Image>> =
        MutableStateFlow(ScreenState.Loading)

    private val imageId: Int = checkNotNull(savedStateHandle["id"])

    init {
        requestImageDetails()
    }

    fun onRetryClicked() {
        requestImageDetails()
    }

    private fun requestImageDetails() {
        viewModelScope.launch {
            _screenState.update { ScreenState.Loading }
            try {
                val images = imageRepository.getImage(imageId)
                _screenState.update { ScreenState.Content(images) }
            } catch (exception: Throwable) {
                exception.printStackTrace()
                _screenState.update { ScreenState.Error(exception) }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // PUBLIC STATES
    ///////////////////////////////////////////////////////////////////////////

    val screenState = _screenState.asStateFlow()

    companion object {
        const val KEY_IMAGE_ID = "id"
    }
}