package de.dkb.dkbhometest.presentation.model

import de.dkb.dkbhometest.data.model.ImageDto

object ImageMapper {
    fun ImageDto.toInternalObject(): Image = Image(
        albumId = this.albumId,
        id = this.id,
        title = this.title,
        // it's needed to replace via.placeholder.com image urls, because it blocks mobile devices
        url = "https://picsum.photos/seed/${this.id}/600",
        thumbnailUrl = "https://picsum.photos/seed/${this.id}/150"
    )
}