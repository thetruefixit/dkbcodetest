@file:OptIn(ExperimentalMaterial3Api::class)

package de.dkb.dkbhometest.presentation.compose.details

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import de.dkb.dkbhometest.R
import de.dkb.dkbhometest.core.compose.NormalGlideImage
import de.dkb.dkbhometest.core.other.ScreenState
import de.dkb.dkbhometest.presentation.compose.basic.ErrorScreen
import de.dkb.dkbhometest.presentation.compose.basic.LoadingScreen
import de.dkb.dkbhometest.presentation.functional.details.DetailsViewModel
import de.dkb.dkbhometest.presentation.model.Image

@Composable
fun DetailsScreen(
    viewModel: DetailsViewModel = hiltViewModel(),
    onBackClick: () -> Unit
) {
    val screenState = viewModel.screenState.collectAsStateWithLifecycle()
    Scaffold(topBar = {
        TopBar(
            stringResource(id = R.string.details_title),
            onBackClick
        )
    }) { padding ->
        when (val currentState = screenState.value) {
            is ScreenState.Content -> ScrollableView(image = currentState.data, padding = padding)
            is ScreenState.Error -> ErrorScreen(
                errorText = stringResource(id = R.string.error_unknown),
                padding = padding
            ) { viewModel.onRetryClicked() }
            ScreenState.Loading -> LoadingScreen(padding = padding)
        }

    }
}

@Composable
private fun TopBar(title: String, onBackClick: () -> Unit) {
    CenterAlignedTopAppBar(
        title = {
            Text(text = title)
        },
        navigationIcon = {
            IconButton(onClick = onBackClick) {
                Icon(Icons.Filled.ArrowBack, contentDescription = "Back")
            }
        },
    )
}

@Composable
private fun ScrollableView(image: Image, padding: PaddingValues) {
    LazyColumn(
        modifier = Modifier
            .padding(padding)
            .fillMaxWidth()
    ) {
        item {
            Spacer(modifier = Modifier.height(16.dp))
            TitleCard(title = image.title)
            Spacer(modifier = Modifier.height(16.dp))
            NormalGlideImage(imageUrl = image.url)
            Spacer(modifier = Modifier.height(64.dp))
        }
    }
}

@Composable
private fun TitleCard(title: String) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.headlineMedium,
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            textAlign = TextAlign.Center
        )
    }
}

@Preview
@Composable
private fun PreviewDetails() {
    DetailsScreen {}
}