package de.dkb.dkbhometest.presentation.compose.basic

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.dkb.dkbhometest.R

@Composable
fun ErrorScreen(
    errorText: String,
    padding: PaddingValues,
    isShowRetryButton: Boolean = true,
    onRetryClicked: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(padding),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(errorText, color = Color.Black, fontSize = 20.sp)
        Spacer(modifier = Modifier.height(16.dp))
        if (isShowRetryButton) {
            Button(onClick = onRetryClicked) {
                Text(stringResource(R.string.button_retry))
            }
        }
    }
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
private fun ErrorPreview() {
    ErrorScreen(
        errorText = stringResource(R.string.error_unknown),
        PaddingValues(10.dp),
        true
    ) {}
}