@file:OptIn(
    ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class,
)

package de.dkb.dkbhometest.presentation.compose.home

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.key
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import de.dkb.dkbhometest.R
import de.dkb.dkbhometest.core.compose.NormalGlideImage
import de.dkb.dkbhometest.core.other.ScreenState
import de.dkb.dkbhometest.presentation.compose.basic.ErrorScreen
import de.dkb.dkbhometest.presentation.compose.basic.LoadingScreen
import de.dkb.dkbhometest.presentation.functional.home.HomeViewModel
import de.dkb.dkbhometest.presentation.model.Image

@Composable
fun HomeScreen(
    viewModel: HomeViewModel = hiltViewModel(), onImageClicked: (Image) -> Unit
) {
    val screenState = viewModel.screenState.collectAsStateWithLifecycle()
    ListScreen(
        screenState = screenState.value,
        onRetryClicked = { viewModel.onRetryClicked() },
        onImageClicked = onImageClicked
    )
}

@Composable
private fun ListScreen(
    screenState: ScreenState<List<Image>>,
    onRetryClicked: () -> Unit,
    onImageClicked: (Image) -> Unit
) {
    Scaffold { padding ->
        when (screenState) {
            is ScreenState.Loading -> LoadingScreen(padding)
            is ScreenState.Content -> {
                ListView(imageList = screenState.data, padding, onImageClicked)
            }
            is ScreenState.Error -> ErrorScreen(
                stringResource(id = R.string.error_unknown), padding, true
            ) { onRetryClicked() }
        }
    }
}

@Composable
private fun ListView(
    imageList: List<Image>, padding: PaddingValues, onImageClicked: (Image) -> Unit
) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        modifier = Modifier.padding(padding),
    ) {
        itemsIndexed(imageList) { key, listItem ->
            key(key) {
                ListItem(listItem, onImageClicked)
            }
        }
    }
}

@Composable
private fun ListItem(image: Image, onImageClicked: (Image) -> Unit) {
    ElevatedCard(
        modifier = Modifier
            .padding(2.dp)
            .size(150.dp)
            .clickable { onImageClicked(image) },
        elevation = CardDefaults.cardElevation(defaultElevation = 6.dp)
    ) {
        NormalGlideImage(imageUrl = image.thumbnailUrl)
    }
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
private fun PreviewContent() {
    ListScreen(screenState = ScreenState.Content(
        listOf(
            Image(1, 1, "Test", "any", "any"),
            Image(1, 1, "Test", "any", "any"),
            Image(1, 1, "Test", "any", "any"),
            Image(1, 1, "Test", "any", "any"),
        )
    ), {}, {})
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
private fun PreviewError() {
    ListScreen(screenState = ScreenState.Error(RuntimeException()), {}, {})
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
private fun PreviewLoading() {
    ListScreen(screenState = ScreenState.Loading, {}, {})
}