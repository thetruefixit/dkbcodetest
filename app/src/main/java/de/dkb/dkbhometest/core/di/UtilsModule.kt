package de.dkb.dkbhometest.core.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.dkb.dkbhometest.utils.CoroutinesManager
import de.dkb.dkbhometest.utils.RealCoroutinesManager
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface UtilsModule {
    @Binds
    @Singleton
    fun bindCoroutinesManager(realCoroutinesManager: RealCoroutinesManager): CoroutinesManager
}