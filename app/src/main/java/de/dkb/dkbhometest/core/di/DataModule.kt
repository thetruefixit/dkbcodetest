package de.dkb.dkbhometest.core.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.dkb.dkbhometest.data.repositories.ImageRepository
import de.dkb.dkbhometest.data.repositories.ImageRepositoryImpl
import de.dkb.dkbhometest.data.sources.ImageDataSource
import de.dkb.dkbhometest.data.sources.RemoteImageDataSource
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface DataModule {
    @Binds
    @Singleton
    fun bindRemoteDataSource(remote: RemoteImageDataSource): ImageDataSource

    @Binds
    @Singleton
    fun bindImageRepository(repositoryImpl: ImageRepositoryImpl): ImageRepository
}
