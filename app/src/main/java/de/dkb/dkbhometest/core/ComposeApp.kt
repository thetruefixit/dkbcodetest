package de.dkb.dkbhometest.core

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import de.dkb.dkbhometest.presentation.compose.details.DetailsScreen
import de.dkb.dkbhometest.presentation.compose.home.HomeScreen
import de.dkb.dkbhometest.presentation.functional.details.DetailsViewModel.Companion.KEY_IMAGE_ID

@Composable
fun ComposeApp() {
    val navController = rememberNavController()
    ApplicationNavHost(
        navController = navController
    )
}

@Composable
fun ApplicationNavHost(
    navController: NavHostController
) {
    NavHost(navController = navController, startDestination = "home") {
        composable("home") {
            HomeScreen {
                navController.navigate("details/${it.id}")
            }
        }
        composable(
            "details/{$KEY_IMAGE_ID}",
            arguments = listOf(navArgument(KEY_IMAGE_ID) { type = NavType.IntType })
        ) {
            DetailsScreen(onBackClick = { navController.navigateUp() })
        }
    }
}