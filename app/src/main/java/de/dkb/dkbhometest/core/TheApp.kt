package de.dkb.dkbhometest.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class TheApp : Application()