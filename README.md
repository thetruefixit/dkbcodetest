DKB Code challenge
========================   

## Architecture

#### Clean Architecture + MVVM + Compose + Hilt + Glide + Retrofit

## Solution

While implementing a code challenge, I encountered an issue with the provided API. The API returned
image URLs that use `via.placeholder.com`, which blocked any mobile connection to the API.

This issue was easily fixable in 2020 by setting a `User-Agent` header.  
[StackOverflow](https://stackoverflow.com/questions/69194656/glide-wont-load-placeholder-com-image-without-extension)
and [Glide GitHub](https://github.com/bumptech/glide/issues/4074)  
However, it seems that the API has since introduced new blocking measures, possibly by requiring
additional headers or blocking mobiles based on their platform.

Due to time constraints, I had to resort to a quick and dirty solution of hardcoding the replacement
of image URLs in the `ImageMapper` class. However, in normal circumstances, I would take the time to
investigate the issue further and find a more sustainable solution that does not require such "dirty
hacks" for URL manipulation.

## Summary

Dear DKB Team,

I am pleased to share with you an application that incorporates several cutting-edge technologies,
including MVVM, Jetpack Compose and Clean Architecture principles.

Although I have not created separate modules for features and components like DI and Navigation due
to time constraints, it would be the recommended approach for better modularity and scalability.

The application is divided into two essential parts, namely the data and presentation layers.  (
Domain is missing)  
Currently, the schema of the app follows the pattern
of `Compose View > ViewModel -> Repository -> DataSource -> API`.

If given more time, I would like to make the following improvements:

- Develop comprehensive testing methods for the application, including ViewModel tests (which I have
  already taken care of) for increased robustness and reliability.
- Implement the domain layer to enhance the overall architecture and code maintainability.
- Introduce multi-module architecture to facilitate the development process and maintain separation
  of concerns.
- Find a way to fix API :)

Thank you for the opportunity to work on this project, and I am looking forward to hearing your
feedback.